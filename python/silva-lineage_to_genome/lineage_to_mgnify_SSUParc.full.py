#!/usr/bin/python3

# From SILVA fasta headers obtain (cleaned, no dot) SILVA-id's and lineage  SILVA-LINEAGE
# sed 's/\./ /' SILVA_138.1_HEADERS| cut -d" " -f1,3- > SILVA-ID_LINEAGE

# From SILVA metadata only obtain SILVA-ID and NCBI-taxonID
# awk '{print $1,$NF}' SILVA_138.1_SSURef.full_metadata > SILVA-ID_NCBI-TAX

# FINAL OUTPUT:
# LINEAGE SILVA <tab> MGnify_ID

# SILVA-LINEAGE
# SILVA-ID_NCBI-TAX
# MagID_TaxID

manual_lineages = {}
for line in open("manual_lineages"):
    manual_lineages[line.split()[0]] = line.strip().split()[1]

silva_lineages = {}
silva_genus = {}
silva_family = {}

with open("SILVA-ID_LINEAGE_SSUParc") as infile:
    for line in infile:
        id = line.split()[0]
        lineage = " ".join(line.strip().split()[1:])
        silva_lineages[id] = lineage

        genus = lineage.split(";")[-2]
        silva_genus[genus] = ";".join(lineage.split(";")[0:-1])

        if not "Eukaryota" in lineage or "Archaea" in lineage:
            if len(lineage.split(";")) > 6:
                family = lineage.split(";")[-3]
                silva_family[family] = ";".join(lineage.split(";")[0:-2])

tax_silva = {}
for line in open("SILVA-ID_NCBI-TAX_SSUParc.full", "r").readlines():
#for line in open("SILVA-ID_NCBI-TAX", "r").readlines():
    sline = line.strip().split()
    taxon = sline[1]
    acc = sline[0]
    if taxon not in tax_silva.keys():
        tax_silva[taxon] = [acc]
    else:
        tax_silva[taxon].append(acc)

c = 0

taxon_hits_conv = {}
nontaxon_hits_conv = {}
all_mags = []
hit = 0
taxhit = 0
notaxhit = 0
for line in open("magID_taxID.tsv", "r").readlines():
    taxon = line.strip().split()[0]
    magID = line.split()[1]
    name = line.strip().split("\t")[-1]
    all_mags.append(magID)

    # direct with Silva database
    if taxon in tax_silva.keys():
        taxhit += 1
        for acc in tax_silva[taxon]:
            if acc in silva_lineages.keys():
                hit += 1
                lineage = silva_lineages[acc]
                if lineage not in taxon_hits_conv.keys():
                    taxon_hits_conv[lineage] = [magID]
                else:
                    taxon_hits_conv[lineage].append(magID)
                break
    # Non direct hit. Using organism name given with the UHGG genome to search in the Silva Lineages
    else:
        uncultured = False
        if "uncultured" in name:
            uname = name.replace("uncultured ", "")
            uncultured = True
        elif "Candidatus" in name:
            uname = name.replace("Candidatus ", "")
        else:
            uname = name

        sname = uname.split()[0]
        lineage = ""
        if sname in silva_genus.keys():
            lineage = silva_genus[sname]
        elif sname in silva_family.keys():
            lineage = silva_family[sname]

        if lineage:
            notaxhit += 1
            if uncultured:
                for lin in silva_lineages.values():
                    if sname+";uncultured bacterium" in lin or sname+";uncultured;uncultured bacterium" in lin:
                        lineage = lin
                        break

        # lookup in the manual curated lineages for the genomes
        elif sname in manual_lineages.keys():
            lineage = manual_lineages[sname]
            notaxhit += 1
        else:
            c += 1
            print(taxon, magID, name, sname)

        if lineage:
            if lineage not in nontaxon_hits_conv.keys():
                nontaxon_hits_conv[lineage] = [magID]
            else:
                nontaxon_hits_conv[lineage].append(magID)

print(len(all_mags), taxhit, notaxhit, c)

taxon_hits_conv_f = open("Silva138.1-Lineage_UHGG-mags_taxonhits.tsv","w")
for lin in taxon_hits_conv:
    taxon_hits_conv_f.write(lin+"\t"+",".join(taxon_hits_conv[lin])+"\n")
taxon_hits_conv_f.close()

nontaxon_hits_conv_f = open("Silva138.1-Lineage_UHGG-mags_non-taxonhits.tsv","w")
for lin in nontaxon_hits_conv:
    nontaxon_hits_conv_f.write(lin+"\t"+",".join(nontaxon_hits_conv[lin])+"\n")
nontaxon_hits_conv_f.close()