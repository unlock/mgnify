#!/usr/bin/python3
from os import listdir
from lxml import etree

wfile = open("magID_taxID.tsv","w")
for xml in listdir("xml"):
    xmlfile = etree.parse("xml/"+xml)
    root= xmlfile.getroot()
    for t in root.iter('TAXON_ID'): taxonID = t.text
    for id in root.iter('SUBMITTER_ID'): magID = id.text
    for id in root.iter('SCIENTIFIC_NAME'): name = id.text

    wfile.write(taxonID+"\t"+magID+"\t"+name+"\n")
wfile.close()
