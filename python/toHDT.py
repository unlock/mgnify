import os

from irods.session import iRODSSession

def execute(command):
    print(command)
    os.system(command)

command = """iquest --no-page "%s/%s" "SELECT COLL_NAME, DATA_NAME WHERE COLL_NAME like '%reference%' and DATA_NAME like '%.interproscan.eggnog.ttl.gz'" > files.txt"""
execute(command)
command = """iquest --no-page "%s/%s" "SELECT COLL_NAME, DATA_NAME WHERE COLL_NAME like '%reference%' and DATA_NAME like '%.interproscan.eggnog.hdt'" > hdt_files.txt"""
execute(command)

hdts = set()
for line in open("hdt_files.txt"):
    hdts.add(line.strip())

print(hdts)

for line in open("files.txt"):
    line = line.strip()
    name = line.split("/")[-1].replace(".ttl.gz","")

    hdt_file = os.path.dirname(line) +"/"+ name + ".hdt"

    if hdt_file in hdts: continue

    print("Generating", hdt_file)

    command = "iget " + line
    execute(command)
    
    command = "java -jar /unlock/infrastructure/binaries/sapp/Conversion-0.1.586.jar -convert -i " + line.split("/")[-1] + " -o " + name + ".hdt"
    execute(command)

    command = "icd " + os.path.dirname(line)
    execute(command)

    command = "iput " + name + ".hdt"
    execute(command)

    # Clean up
    execute("rm " + name + ".hdt")
    execute("rm " + line.split("/")[-1])