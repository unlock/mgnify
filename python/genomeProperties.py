"""
Author: Jasper Koehorst
Script to build genome properties output from an HDT file
"""
import csv
import logging
import os
import re
import sys
from os.path import splitext, basename

import pandas as pd
import requests
from joblib import Parallel, delayed
from pygenprop import assign
from pygenprop.database_file_parser import parse_genome_properties_flat_file
from pygenprop.results import GenomePropertiesResults
from rdflib import Graph
from rdflib_hdt import optimize_sparql, HDTStore

from collections import defaultdict

print("Loading genome properties")

kmer_size = 20

num_cores = 2  # multiprocessing.cpu_count()



def get_genome_properties():
    genome_properties_database_url = 'https://raw.githubusercontent.com/ebi-pf-team/genome-properties/master/flatfiles/genomeProperties.txt'

    if not os.path.isfile('genomeProperties.txt'):
        logging.info('Downloading the genome properties file')

        req = requests.get(genome_properties_database_url)

        # open method will open a file on your system and write the contents
        with open('genomeProperties.txt', 'wb') as f:
            f.write(req.content)

    content = open('genomeProperties.txt').readlines()
    global tree
    tree = parse_genome_properties_flat_file(content)


def main(hdt_file):
    get_genome_properties()
    prot_tsv_file = export_tsv(hdt_file)
    genome_properties(prot_tsv_file)
    


def export_tsv(hdt_file):
    print("Exporting", hdt_file, "to tsv")
    # Calling this function optimizes the RDFlib SPARQL engine for HDT documents
    optimize_sparql()

    graph = Graph(store=HDTStore(hdt_file))

    samplesResults = graph.query("""
        PREFIX gbol: <http://gbol.life/0.1/>
        SELECT DISTINCT ?sample ?samplename
        WHERE { 
            ?sample a gbol:Sample .
            ?sample gbol:name ?samplename .
        }""")

    if not os.path.exists('prot_tsv/'):
        os.makedirs('prot_tsv', exist_ok=True)
        # subprocess.check_output('mkdir prot_tsv', shell=True)

    # Multiple samples possible
    sample_store = {}
    for sampleRow in samplesResults:
        sample_name = f"{sampleRow.samplename}"

        outFileName = 'prot_tsv/' + sample_name + ".tsv"
        
        # If output file exists, skip this sample
        if os.path.isfile(outFileName): continue

        print('%s' % sample_name)

        sampleIRI = (f"{sampleRow.sample}")

        interproResults = graph.query("""
            PREFIX gbol: <http://gbol.life/0.1/>
            SELECT DISTINCT ?contig ?gene ?strand ?gbeginpos ?gendpos ?cdsbeginpos ?cdsendpos ?featurebeginpos ?featureendpos ?acc ?singnaturedes ?dbname ?evalue
            WHERE {
                ?sample a gbol:Sample .
                ?contig gbol:sample ?sample .
                VALUES ?sample {<""" + sampleIRI + """>}
                ?contig gbol:feature ?gene.
                ?gene gbol:location ?glocation .
                ?glocation gbol:begin ?gbegin .
                ?gbegin gbol:position ?gbeginpos .
                ?glocation gbol:end ?gend .
                ?gend gbol:position ?gendpos .
                ?glocation gbol:strand ?strand .
                ?gene gbol:transcript ?mrna .
                ?mrna gbol:feature ?cds .
                # ?cds gbol:location ?cdslocation .
                # ?cdslocation gbol:begin ?cdsbegin .
                # ?cdsbegin gbol:position ?cdsbeginpos .
                # ?cdslocation gbol:end ?cdsend .
                # ?cdsend gbol:position ?cdsendpos .
                ?cds gbol:protein ?prot .
                ?prot gbol:feature ?feature .
                ?feature gbol:signature ?signature .
                ?signature gbol:accession ?acc .
                ?signature gbol:db ?db .
                ?db gbol:id ?dbname .
                ?feature gbol:signatureDesc ?singnaturedes .
                # ?feature gbol:location ?featurelocation .
                # ?featurelocation gbol:begin ?featurebegin .
                # ?featurebegin gbol:position ?featurebeginpos .
                # ?featurelocation gbol:end ?featureend .
                # ?featureend gbol:position ?featureendpos .
                ?feature gbol:provenance ?provenance .
                ?provenance gbol:annotation ?annot .
                ?annot gbol:evalue ?evalue .
            }""")

        data = []
        for interproRow in interproResults:
            contig = f"{interproRow.contig}"
            gene = f"{interproRow.gene}"
            genename = re.sub(r'gene/.*', '', gene)
            strand = f"{interproRow.strand}"
            strandname = re.sub(r'http://gbol\.life/0\.1/', '', strand)
            gbeginpos = f"{interproRow.gbeginpos}"
            gendpos = f"{interproRow.gendpos}"
            # cdsbeginpos = f"{interproRow.cdsbeginpos}"
            # cdsendpos = f"{interproRow.cdsendpos}"
            # featurebeginpos = f"{interproRow.featurebeginpos}"
            # featureendpos = f"{interproRow.featureendpos}"
            acc = f"{interproRow.acc}"
            singnaturedes = f"{interproRow.singnaturedes}"
            dbname = f"{interproRow.dbname}"
            evalue = f"{interproRow.evalue}"
            data += [(sample_name, contig, int(gbeginpos), int(gendpos), strandname, acc, singnaturedes, dbname,
                      float(evalue))]
        data.sort()
        sample_store[sample_name] = data

    for sample_name in sample_store:
        outFile = open(outFileName, 'w')
        for element in sample_store[sample_name]:
            outFile.write('\t'.join(str(x) for x in element) + '\n')
        outFile.close()

    return outFileName


def genome_properties(input_file):
    print("Processing", input_file, "to genome properties")
    if not os.path.isdir("./gps/"): os.mkdir("./gps")
    
    gp_file = "./gps/" + input_file.split("/")[-1].split(".")[0] + ".gp.tsv"
    if os.path.isfile(gp_file): return

    with open(input_file) as interproscan_file:
        final_results = {}
        identifiers = []
        i = 0
        tsv_reader = csv.reader(interproscan_file, delimiter='\t')
        partial = set()
        yes = set()

        for index, row in enumerate(tsv_reader):
            matched_interpro_member_database_id = row[5]
            identifiers.append(matched_interpro_member_database_id)
            i += 1
            if i == kmer_size:
                assignment_cache = assign.AssignmentCache(interpro_signature_accessions=identifiers,
                                                          sample_name=splitext(basename(interproscan_file.name))[0])
                results = GenomePropertiesResults(assignment_cache, properties_tree=tree)
                gp_results_dict = results.property_results.to_dict()[input_file.split("/")[-1].split('.tsv')[0]]
                gp_results_dict_inverted = defaultdict(list)
                {gp_results_dict_inverted[v].append(k) for k, v in gp_results_dict.items()}
                yes.update(gp_results_dict_inverted['YES'])
                partial.update(gp_results_dict_inverted['PARTIAL'])
                i -= 1
            if index % 100 == 0:
                print(index)


    # Create a dict with PARTIAL AND YES
    partial = partial - yes
    gp_results_dict = dict.fromkeys(yes, "YES")
    gp_results_dict.update(dict.fromkeys(partial, "PARTIAL"))
    # print(gp_results_dict)

    df = pd.DataFrame.from_dict(gp_results_dict, orient='index', columns=[input_file.split('.tsv')[0]])
    # print(df)

    df.columns = [input_file.split("/")[-1].split(".")[0]]
    content = df.to_csv(sep="\t")
    output = open(gp_file, "w")
    output.write(content)
    output.close()

    #     file = path + "/" + file

    #     if os.path.isfile(gp_file): return
    #     if not os.path.isfile(file): return

    #     print("Processing " + file)

    # #     loader = importlib.machinery.SourceFileLoader('report', '../data/git/mgnify/bla_GPs/pygenprop/run_pygenprop_modified_kmer_no_strand.py')
    # #     handle = loader.load_module('report')
    # #     sample_store = handle.main(file, 20)

    #     df.columns = [file.split("/")[-1].split(".")[0]]
    #     content = df.to_csv(sep="\t")
    #     output = open(gp_file, "w")
    #     output.write(content)
    #     output.close()

def parallel(hdt_files):
    process = set()
    for hdt_file in hdt_files:
        if hdt_file.endswith(".hdt"):
            process.add(hdt_file)

    # Parallel function
    results = Parallel(n_jobs=num_cores)(delayed(main)(i) for i in sorted(process))

if __name__ == "__main__":
    # If sys.argv[1] == a dir
    if len(sys.argv) == 1:
        sys.argv[1] = "../hdt"
    if os.path.isdir(sys.argv[1]):
        sys.argv[1] += "/"
        hdt_files = os.listdir(sys.argv[1])
        for index, hdt_file in enumerate(hdt_files):
            hdt_files[index] = sys.argv[1] + hdt_file
        # parallel(hdt_files)
        # Collect the outputs
        
        # Taxon lineage
        mgnify_lineage = {}
        for line in open("silva-lineage_to_genome/Silva138.1-Lineage_UHGG-mags_merged.tsv"):
            lineage, mgygs = line.split("\t")
            for mgyg in mgygs.split(","):
                mgnify_lineage[mgyg.strip()] = lineage
        print("mgnify_lineage", len(mgnify_lineage))

        # Genome property into matrix
        matrix = []
        for file in os.listdir("./gps"):
            lines = open("./gps/" + file).readlines()
            for index, line in enumerate(lines):
                if index == 0:
                    header = lines[index].strip()
                    if header in mgnify_lineage:
                        matrix.append([header, "taxonomy", mgnify_lineage[header]])
                    else:
                        print(header, " is unknown")
                        matrix.append([header, "taxonomy", "unknown"])
                else:
                    key, value = line.strip().split()
                    matrix.append([header, key, value])
        # Turn into panda matrix
        df = pd.DataFrame(matrix, columns=['X','Y','Z'])
        df = df.pivot(index='X', columns='Y', values='Z')
        df.fillna('NO', inplace=True)

        df.to_csv("matrix.tsv", sep="\t")

            
            
    else:
        # hdt_file = "../hdt/MGYG-HGUT-01159.interproscan.eggnog.hdt"  # sys.argv[1]
        main(sys.argv[1])

