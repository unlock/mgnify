package nl.munlock.main;

import nl.munlock.eggnog.Eggnog;
import nl.munlock.interproscan.InterProScan;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {

        List<String> argsList = new LinkedList<>(Arrays.asList(args));

        if (argsList.indexOf("-tool") == -1) {
            throw new Exception("-tool not found, use -tool in conjunction with 'conversion', 'interpro' or 'eggnog'");
        }

        String tool = argsList.get(argsList.indexOf("-tool") + 1);
        if (tool.matches("conversion")) {
            argsList.remove(argsList.indexOf("-tool") + 1);
            argsList.remove(argsList.indexOf("-tool"));
            nl.wur.ssb.conversion.App.main(argsList.toArray(new String[0]));
        } else if (tool.matches("interpro")) {
            // nl.munlock.interproscan.App.main(args);
            new InterProScan(args);
        } else if (tool.matches("eggnog")) {
            new Eggnog(args);
        } else {
            throw new Exception("Tool " + tool + " not recognised");
        }
    }
}
