package nl.munlock.eggnog;

import life.gbol.domain.*;
import nl.munlock.eggnog.CommandOptions;

import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.Xrefs;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.purl.ontology.bibo.domain.Document;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.UUID;

public class Eggnog  extends SequenceBuilder {

    private XRefProvenance xrefProv;
    private CommandOptions arguments;
    private File name;
    private final Logger logger = Logger.getLogger(Eggnog.class);
    final UUID xrefprovID = UUID.randomUUID();

    public Eggnog(String[] args) throws Exception {
        super(null, "http://gbol.life/0.1/");

        arguments = new CommandOptions(args);

        this.domain = arguments.domain;

        // store of the final output...
        final File output = arguments.output;

        name = new File(arguments.input.get(0));

        this.rootIRI = "http://gbol.life/0.1/" + xrefprovID + "/";

        xrefProv = arguments.domain.make(XRefProvenance.class, rootIRI + "XRefProv");

        // xrefProv.setOrigin(arguments.annotResult);

        Generic.Logger(arguments.debug);
        // If no sequences are found the analysis is obviously skipped
        // Everything is now from loadRDF as we have set a limit to 5.000 per
        // analysis run...
        parser(new File(arguments.eggnogFile));

        logger.info("Results are saved in: " + arguments.output.getAbsolutePath());
        nl.wur.ssb.SappGeneric.InputOutput.Output.save(arguments.domain, arguments.output);

        arguments.domain.close();
    }

    private void parser(File file) throws Exception {
        logger.debug("Parsing the MGnify TSV file");

        // Obtain protein sequences and calculate MD5
        Iterator<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery("getProteins.txt", true).iterator();
        HashMap<String, String> locusLookup = new HashMap<>();
        while (resultLines.hasNext()) {
            ResultLine resultLine = resultLines.next();
            String iri = resultLine.getIRI("protein");
            String locus = resultLine.getLitString("locus");
            locusLookup.put(locus.toUpperCase(), iri);
        }

        Scanner scanner = new Scanner(file);

        int annotationParsed = 0;
        while (scanner.hasNext()) {
            annotationParsed++;
            if (annotationParsed % 100 == 0)
                System.out.print("Annotations parsed: " + annotationParsed + "\r");

            String line = scanner.nextLine();

            if (line.startsWith("#"))
                continue;

            String[] lineArray = line.split("\t");

            //#query_name
            String query_name = lineArray[0]; // done
            String seed_eggNOG_ortholog = lineArray[1]; // done
            String seed_ortholog_evalue = lineArray[2]; // done
            String seed_ortholog_score = lineArray[3]; // done
            String best_tax_level = lineArray[4];
            String Preferred_name = lineArray[5];
            String GOs = lineArray[6]; // xref
            String EC = lineArray[7]; // xref
            String KEGG_ko = lineArray[8]; // xref
            String KEGG_Pathway = lineArray[9]; //xref
            String KEGG_Module = lineArray[10]; //xref
            String KEGG_Reaction = lineArray[11]; //xref
            String KEGG_rclass = lineArray[12]; //xref
            String BRITE = lineArray[13]; //xref
            String KEGG_TC = lineArray[14]; //xref
            String CAZy = lineArray[15]; //xref
            String BiGG_Reaction = lineArray[16]; //xref

            String proteinURI = locusLookup.get(query_name);

            if (proteinURI == null) {
                throw new Exception("Null found!? for " + file);
            }

            Protein proteinRDF = arguments.domain.make(life.gbol.domain.Protein.class, proteinURI);

            // System.err.println(StringUtils.join(lineArray, "\n"));

            // Convert to RDF! - protein URI + seed_eggNOG_ortholog identifier
            ProteinDomain proteinDomain = arguments.domain.make(ProteinDomain.class, proteinRDF.getResource().getURI() + "/" + seed_eggNOG_ortholog);

            // Annotation result
            AnnotationResult annotationResult = arguments.domain.make(AnnotationResult.class, arguments.annotResultIRI);

            // Provenance
            String version = "1.0-MGnify";
            String library = "eggNOG";

            life.gbol.domain.FeatureProvenance featureprov = arguments.domain.make(life.gbol.domain.FeatureProvenance.class, proteinDomain.getResource().getURI() + "/" + library + "/" + version);

            nl.systemsbiology.semantics.sapp.domain.InterProScan provannot = arguments.domain.make(nl.systemsbiology.semantics.sapp.domain.InterProScan.class, proteinDomain.getResource().getURI() + "/" + library + "/" + version + "/prov"); // scoretry {

            if (seed_ortholog_evalue.contains(";")) {
                provannot.setEvalue(Double.valueOf(seed_ortholog_evalue.split(";")[1]));
            } else {
                provannot.setEvalue(Double.valueOf(seed_ortholog_evalue));
            }

            if (seed_ortholog_score.contains(";"))
                provannot.setScore(Double.valueOf(seed_ortholog_score.split(";")[1]));
            else {
                provannot.setScore(Double.valueOf(seed_ortholog_score));
            }

            Document document = domain.make(Document.class, "https://blabla.com");
            document.setTitle("EGNOGG BLA");
            document.setAbstract("ABSTRACT HERE");
            // document.setDateAccepted(LocalDate.parse("2020-11-06").atStartOfDay());
            document.setDoi("DOI HERE");
            provannot.setReference(document);

            featureprov.setAnnotation(provannot);
            featureprov.setOrigin(annotationResult);
            proteinDomain.addProvenance(featureprov);

            XRefProvenance xrefprov = arguments.domain.make(life.gbol.domain.XRefProvenance.class, featureprov.getResource().getURI() + "/xrefprov");
            xrefprov.setOrigin(annotationResult);

            for (String go_annotation : GOs.split(",")) {
                if (go_annotation.length() == 0) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "go", version, go_annotation, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ec_annotation : EC.split(",")) {
                if (ec_annotation.length() == 0) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "ec", version, ec_annotation, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : KEGG_ko.split(",")) {
                if (ko.length() == 0) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.orthology", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : KEGG_Module.split(",")) {
                if (ko.length() == 0) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.module", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : KEGG_Pathway.split(",")) {
                if (ko.length() == 0) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.pathway", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : KEGG_Reaction.split(",")) {
                if (ko.length() == 0) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.reaction", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : KEGG_rclass.split(",")) {
                if (ko.length() == 0) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.reaction", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : BRITE.split(",")) {
                if (ko.length() == 0) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "brite", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : KEGG_TC.split(",")) {
                if (ko.length() == 0) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.tc", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String cazy : CAZy.split(",")) {
                if (cazy.length() == 0) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "cazy", version, cazy, null);
                System.err.println(xref.getResource().getURI());
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String bigg : BiGG_Reaction.split(",")) {
                if (bigg.length() == 0) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "bigg.reaction", version, bigg, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }
        }
    }
}
