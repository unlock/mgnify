package nl.munlock.eggnog;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;
import nl.wur.ssb.SappGeneric.ImportProv;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.UUID;

@Parameters(commandDescription = "Available options: ")
public class CommandOptions extends CommandOptionsGeneric {
    private static final Logger logger = Logger.getLogger(CommandOptions.class);

    public String annotResultIRI;

    @Parameter(names = {"-t", "-tool"},  description = "Which conversion tool to use, (conversion, interpro, eggnog, antismash)", required = true)
    String tool;

    @Parameter(names = {"-tsv"}, description = "MGnify TSV eggnog file", required = true)
    String eggnogFile;

    @Parameter(names = {"-v", "-version"}, description = "Version of eggnog used (name should match folder name in the database folder)", required = true)
    String toolversion;

    private String commandLine;
    private final String repository = "https://gitlab.com/sapp/annotation/eggnog";

    final String description = "Eggnog annotation module of SAPP";
    public AnnotationResult annotResult;

    @Parameter(names = "-starttime", description = "Start time of code", hidden = true)
    private LocalDateTime starttime = LocalDateTime.now();

    // TODO this is the default CommandOptions code ...
    public CommandOptions(String args[]) throws Exception {
        try {
            new JCommander(this, args);
            this.commandLine = StringUtils.join(args, " ");

            if (this.help || args.length == 0)
                throw new ParameterException("");

            String[] files = new String[0];

            if (this.input != null) {
                logger.info("Loading data from file");
                files = new String[]{this.input.get(0)};
                this.domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(this.input);
            }

            if (this.input != null) {
                files = new String[]{this.input.get(0)};
            }

            ImportProv origin = new ImportProv(domain, Arrays.asList(files), this.output, this.commandLine, this.tool, this.toolversion, this.repository, null, this.starttime, LocalDateTime.now());

            annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());
            annotResultIRI = annotResult.getResource().getURI();
            origin.linkEntity(annotResult);

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
