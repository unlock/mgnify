package nl.munlock.interproscan;

import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.Xrefs;
import org.apache.log4j.Logger;
import org.purl.ontology.bibo.domain.Document;

import java.io.File;
import java.util.*;

public class InterProScan extends SequenceBuilder {

    private XRefProvenance xrefProv;
    private CommandOptions arguments;
    private File name;
    private final Logger logger = Logger.getLogger(InterProScan.class);
    final UUID xrefprovID = UUID.randomUUID();

    public InterProScan(String[] args) throws Exception {
        super(null, "http://gbol.life/0.1/");

        arguments = new CommandOptions(args);

        this.domain = arguments.domain;

        // store of the final output...
        final File output = arguments.output;

        name = new File(arguments.input.get(0));

        this.rootIRI = "http://gbol.life/0.1/" + xrefprovID + "/";

        xrefProv = arguments.domain.make(XRefProvenance.class, rootIRI + "XRefProv");

        // xrefProv.setOrigin(arguments.annotResult);

        Generic.Logger(arguments.debug);
        // If no sequences are found the analysis is obviously skipped
        // Everything is now from loadRDF as we have set a limit to 5.000 per
        // analysis run...
        parser(new File(arguments.interproFile));

        logger.info("Results are saved in: " + arguments.output.getAbsolutePath());
        nl.wur.ssb.SappGeneric.InputOutput.Output.save(arguments.domain, arguments.output);

        arguments.domain.close();
    }

    public void parser(File proteinFile) throws Exception {
        logger.debug("Parsing the MGnify TSV file");

        // Obtain protein sequences and calculate MD5
        Iterator<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery("getProteins.txt", true).iterator();
        HashMap<String, String> locusLookup = new HashMap<>();
        while (resultLines.hasNext()) {
            ResultLine resultLine = resultLines.next();
            String iri = resultLine.getIRI("protein");
            String locus = resultLine.getLitString("locus");
            locusLookup.put(locus.toUpperCase(), iri);
        }

        Scanner scanner = new Scanner(proteinFile);

        int annotationParsed = 0;
        while (scanner.hasNext()) {
            annotationParsed++;
            if (annotationParsed % 100 == 0)
                System.out.print("Annotations parsed: " + annotationParsed + "\r");

            String line = scanner.nextLine();

            // System.err.println(line);

            /*
             * Protein object initialization
             */

            // TODO validate if needed when running locally
            String proteinURI = locusLookup.get(line.split("\t")[0]);

            Protein proteinRDF = arguments.domain.make(life.gbol.domain.Protein.class, proteinURI);

            // GUT_GENOME000001_02152	GUT_GENOME000001_02152;a8e6141af44c1da108faec4c9993a699	486	PRINTS	PR02008	RNA (C5-cytosine) methyltransferase signature	467	GUT_GENOME000001_02152;484	9.1E-29	T	20-06-2019	IPR023267	RNA (C5-cytosine) methyltransferase
            String[] lineArray = line.split("\t");

//            Protein accession (e.g. P51587)
            String protein_accession = lineArray[0];
//            Sequence MD5 digest (e.g. 14086411a2cdf1c4cba63020e1622579)
            String md5 = lineArray[1];
//            Sequence length (e.g. 3418)
            String length = lineArray[2];
//            Analysis (e.g. Pfam / PRINTS / Gene3D)
            String analysis = lineArray[3];
//            Signature accession (e.g. PF09103 / G3DSA:2.40.50.140)
            String signature_accession = lineArray[4];
//            Signature description (e.g. BRCA2 repeat profile)
            String signature_description = lineArray[5];
//            Start location
            String start_location = lineArray[6];
//            Stop location
            String stop_location;
            if (lineArray[7].contains(";"))
                stop_location = lineArray[7].split(";")[1];
            else
                stop_location = lineArray[7];
//            Score - is the e-value (or score) of the match reported by member database method (e.g. 3.1E-52)
            String score = lineArray[8];
//            Status - is the status of the match (T: true)
            String status = lineArray[9];
//            Date - is the date of the run
            String date = lineArray[10];
//            InterPro annotations - accession (e.g. IPR002093)
            if (lineArray.length == 11) continue;
            String interpro_accession = lineArray[11];
//            InterPro annotations - description (e.g. BRCA2 repeat)
            if (lineArray.length == 12) continue;
            String interpro_description = lineArray[12];
//            (GO annotations (e.g. GO:0005515) - optional column; only displayed if –goterms option is switched on)
            if (lineArray.length == 13) continue;
            String[] go_annotations = lineArray[13].split("\\|");
//            (Pathways annotations (e.g. REACT_71) - optional column; only displayed if –pathways option is switched on)
            if (lineArray.length == 14) continue;
            String[] pathway_annotations = lineArray[14].split("\\|");

            // Convert to RDF!
            ProteinDomain proteinDomain = arguments.domain.make(ProteinDomain.class, proteinRDF.getResource().getURI() + "/" + signature_accession + "/" + start_location + "-" + stop_location);

            // Annotation result
            AnnotationResult annotationResult = arguments.domain.make(AnnotationResult.class, arguments.annotResultIRI);

            // Provenance
            String version = "1.0-MGnify";
            //            Analysis (e.g. Pfam / PRINTS / Gene3D)
            String library = analysis;
            life.gbol.domain.FeatureProvenance featureprov = arguments.domain.make(life.gbol.domain.FeatureProvenance.class, proteinDomain.getResource().getURI() + "/" + library + "/" + version);

            nl.systemsbiology.semantics.sapp.domain.InterProScan provannot = arguments.domain.make(nl.systemsbiology.semantics.sapp.domain.InterProScan.class, proteinDomain.getResource().getURI() + "/" + library + "/" + version + "/prov"); // score
            try {
                Double.valueOf(score);
                if (Double.valueOf(score) < 1) {
                    provannot.setEvalue(Double.valueOf(score));
                    provannot.setScore(Double.valueOf(-1));
                } else {
                    provannot.setScore(Double.valueOf(score));
                }
            } catch (Exception ex){
                provannot.setScore(Double.valueOf(-1));
            }

            Document document = domain.make(Document.class, "https://doi.org/10.1093/nar/gkaa977");
            document.setTitle("The InterPro protein families and domains database: 20 years on");
            document.setAbstract("The InterPro database (https://www.ebi.ac.uk/interpro/) provides an integrative classification of protein sequences into families, and identifies functionally important domains and conserved sites. InterProScan is the underlying software that allows protein and nucleic acid sequences to be searched against InterPro's signatures. Signatures are predictive models which describe protein families, domains or sites, and are provided by multiple databases. InterPro combines signatures representing equivalent families, domains or sites, and provides additional information such as descriptions, literature references and Gene Ontology (GO) terms, to produce a comprehensive resource for protein classification. Founded in 1999, InterPro has become one of the most widely used resources for protein family annotation. Here, we report the status of InterPro (version 81.0) in its 20th year of operation, and its associated software, including updates to database content, the release of a new website and REST API, and performance improvements in InterProScan.\n");
            // document.setDateAccepted(LocalDate.parse("2020-11-06").atStartOfDay());
            document.setDoi("https://doi.org/10.1093/nar/gkaa977");
            provannot.setReference(document);

            featureprov.setAnnotation(provannot);
            featureprov.setOrigin(annotationResult);
            proteinDomain.addProvenance(featureprov);

//            Protein accession (e.g. P51587)
//            Sequence MD5 digest (e.g. 14086411a2cdf1c4cba63020e1622579)
//            Sequence length (e.g. 3418)

            Database database = arguments.domain.make(life.gbol.domain.Database.class, "http://gbol.life/0.1/database/" + library + "/" + version);
            database.setVersion(version);
            database.setId(library);

//            Signature accession (e.g. PF09103 / G3DSA:2.40.50.140)
            XRefProvenance xrefprov = arguments.domain.make(life.gbol.domain.XRefProvenance.class, featureprov.getResource().getURI() + "/xrefprov");
            xrefprov.setOrigin(annotationResult);
            XRef xref = Xrefs.create(life.gbol.domain.XRef.class, arguments.domain, xrefprov, library.replaceAll("_", ""),version, signature_accession, null);
            xref.setProvenance(xrefprov);

            // proteinDomain.addAccession(signature_accession);
            proteinDomain.setSignature(xref.getResource().getURI());
            proteinDomain.addXref(xref);

//            Signature description (e.g. BRCA2 repeat profile)
            if (signature_description.length() >0)
                proteinDomain.setSignatureDesc(signature_description);

//            Start location
//            Stop location
            Region region = this.makeRegion(Long.parseLong(start_location), Long.parseLong(stop_location), "region", proteinDomain);
            proteinDomain.setLocation(region);

//            Score - is the e-value (or score) of the match reported by member database method (e.g. 3.1E-52)

//            Status - is the status of the match (T: true)
//            Date - is the date of the run

//            InterPro annotations - accession (e.g. IPR002093)
//            featureprov = arguments.domain.make(life.gbol.domain.FeatureProvenance.class, proteinDomain.getResource().getURI() + "/interpro/" + arguments.toolversion);
//            xrefprov = arguments.domain.make(life.gbol.domain.XRefProvenance.class, featureprov.getResource().getURI() + "/xrefprov");
//            xrefprov.setOrigin(annotationResult);

            xref = Xrefs.create(life.gbol.domain.XRef.class, arguments.domain, xrefprov, "interpro", version, interpro_accession, null);
            xref.setProvenance(xrefprov);
            proteinDomain.addSecondarySignature(xref.getResource().getURI());
            proteinDomain.addXref(xref);

            // Create all XREFS, signature and secondary signature already finished
            for (String go_annotation : go_annotations) {
                if (go_annotation.length() == 0) continue;
                xref = Xrefs.create(life.gbol.domain.XRef.class, arguments.domain, xrefprov, "go", version, go_annotation, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String pathway_annotation : pathway_annotations) {
                String pathwayDatabaseName = pathway_annotation.split(":")[0].trim();
                String pathwayIdentifier = pathway_annotation.split(":")[1].trim();

                // Adds more then one kegg EC number using a split / loop
                if (pathwayDatabaseName.matches("KEGG")) {
                    String[] ids = pathwayIdentifier.split("\\+");

                    for (String id : ids) {
                        if (id.matches("^[0-9]+$")) continue;

                        XRef pathwayXref = Xrefs.create(life.gbol.domain.XRef.class, domain, xrefprov,"ec",null, id, null);
                        proteinRDF.addXref(pathwayXref);
                        proteinDomain.addXref(pathwayXref);
                    }
                } else {
                    XRef pathwayXref = Xrefs.create(life.gbol.domain.XRef.class, arguments.domain, xrefprov, pathwayDatabaseName, null, pathwayIdentifier, null);
                    // TODO set pathwayName!
                    proteinRDF.addXref(pathwayXref);
                    proteinDomain.addXref(pathwayXref);
                }
            }

            proteinRDF.addFeature(proteinDomain);
        }
    }
}
