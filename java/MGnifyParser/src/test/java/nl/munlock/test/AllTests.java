package nl.munlock.test;

import junit.framework.TestCase;
import nl.munlock.main.App;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.SappGeneric.XRefDBEntry;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;


public class AllTests extends TestCase {
    private RDFSimpleCon RDFResource;

    public void testConversion() throws Exception {
        File fasta = new File("./src/test/resources/input/MGYG-HGUT-00001.fna");
        File gff = new File("./src/test/resources/input/MGYG-HGUT-00001.gff");
        File output = new File("./src/test/resources/output/MGYG-HGUT-00001.ttl");
        String[] args = {
                "-tool", "conversion",
                "-f", fasta.getAbsolutePath(),
                "-gff2rdf",
                "-i", gff.getAbsolutePath(),
                "-o", output.getAbsolutePath(),
                "-id", "MGYG-HGUT-00001",
//                "-debug",
                "-codon", "11",
                "-topology", "linear"
        };

        App.main(args);
    }

    public void testInterProScan() throws Exception {
        File input = new File("./src/test/resources/output/MGYG-HGUT-00001.ttl");
        File interproscan = new File("./src/test/resources/input/MGYG-HGUT-00001_InterProScan.tsv");
        File output = new File("./src/test/resources/output/MGYG-HGUT-00001-with-interproscan.ttl");
        String[] args = new String[]{"-input", input.getAbsolutePath(), "-output", output.getAbsolutePath(), "-tsv", interproscan.getAbsolutePath(), "-tool", "interpro", "-version", "InterProScan-v5_35-74_0"};
        App.main(args);
    }

    public void testEggnog() throws Exception {
        File input = new File("./src/test/resources/output/MGYG-HGUT-00001.ttl");
        File eggnog = new File("./src/test/resources/input/MGYG-HGUT-00001_eggNOG.tsv");
        File output = new File("./src/test/resources/output/MGYG-HGUT-00001-with-eggNOG.ttl");
        String[] args = new String[]{"-input", input.getAbsolutePath(), "-output", output.getAbsolutePath(), "-tsv", eggnog.getAbsolutePath(), "-tool", "eggnog", "-version", "X.X"};
        App.main(args);
    }

    public void testLineUp() throws Exception {
        File fasta = new File("./src/test/resources/input/MGYG-HGUT-00001.fna");
        File gff = new File("./src/test/resources/input/MGYG-HGUT-00001.gff");
        File output = new File("./src/test/resources/output/MGYG-HGUT-00001.ttl");
        String[] args = {"-tool", "conversion", "-f", fasta.getAbsolutePath(), "-gff2rdf", "-i", gff.getAbsolutePath(), "-o", output.getAbsolutePath(), "-id", "MGYG-HGUT-00001", "-codon", "11", "-topology", "linear"};

        App.main(args);

        File input = new File("./src/test/resources/output/MGYG-HGUT-00001.ttl");
        File interproscan = new File("./src/test/resources/input/MGYG-HGUT-00001_InterProScan.tsv");
        output = new File("./src/test/resources/output/MGYG-HGUT-00001-with-interproscan.ttl");
        args = new String[]{"-input", input.getAbsolutePath(), "-output", output.getAbsolutePath(), "-tsv", interproscan.getAbsolutePath(), "-tool", "interpro", "-version", "InterProScan-v5_35-74_0"};

        App.main(args);

        input = new File("./src/test/resources/output/MGYG-HGUT-00001-with-interproscan.ttl");
        File eggnog = new File("./src/test/resources/input/MGYG-HGUT-00001_eggNOG.tsv");
        output = new File("./src/test/resources/output/MGYG-HGUT-00001-with-interproscan-and-eggNOG.ttl");
        args = new String[]{"-input", input.getAbsolutePath(), "-output", output.getAbsolutePath(), "-tsv", eggnog.getAbsolutePath(), "-tool", "eggnog", "-version", "X.X"};
        App.main(args);
    }

    public void testFolderLineUp() throws Exception {
        File folder = new File("/Users/jasperk/gitlab/m-unlock/mgnify/genome");
        for (Object nameObject : Arrays.stream(folder.list()).sorted().toArray()) {
            try {
                String name = (String) nameObject;

                if (!name.contains("MGYG-HGUT-02884")) continue;

                if (new File(folder + "/" + name).isHidden()) continue;

                String path = folder + "/" + name + "/" + name;

                File fasta = new File(path + ".fna");
                File gff = new File(path + ".gff");
                File output = new File(path + ".ttl");
                String[] args = {"-tool", "conversion", "-f", fasta.getAbsolutePath(), "-gff2rdf", "-i", gff.getAbsolutePath(), "-o", output.getAbsolutePath(), "-id", "MGYG-HGUT-00001", "-codon", "11", "-topology", "linear"};

                if (!output.exists())
                    App.main(args);

                File input = new File(path + ".ttl");
                File interproscan = new File(path + "_InterProScan.tsv");
                output = new File(path + "-with-interproscan.ttl");
                args = new String[]{"-input", input.getAbsolutePath(), "-output", output.getAbsolutePath(), "-tsv", interproscan.getAbsolutePath(), "-tool", "interpro", "-version", "InterProScan-v5_35-74_0"};

                if (!output.exists())
                    App.main(args);

                input = new File(path + "-with-interproscan.ttl");
                File eggnog = new File(path + "_eggNOG.tsv");
                output = new File(path + "-with-interproscan-and-eggNOG.ttl");
                args = new String[]{"-input", input.getAbsolutePath(), "-output", output.getAbsolutePath(), "-tsv", eggnog.getAbsolutePath(), "-tool", "eggnog", "-version", "X.X"};

                if (!output.exists())
                    App.main(args);
            } catch (Exception e) {
                System.err.println("Job failed for...");
            }
        }
    }

    public void testIdentifiers() throws Exception {
        HashMap<String, XRefDBEntry> dbEntries = new HashMap();

        dbEntries.put("<empty>", new XRefDBEntry("No uriRegexPattern available", "No PrimaryRegexPattern available", "", "No id available", "", "No title available", "No description available"));

            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(Util.getResourceFile("identifiers.tsv")));
                int lineCount = 0;

                while(br.ready()) {
                    String line = br.readLine();
                    ++lineCount;
                    String[] lineSplit = line.split("\t");
                    if (lineSplit.length <= 2) {
                        throw new RuntimeException("This is an incomplete line: " + lineCount + " " + line);
                    }

                    String databaseName = lineSplit[0].toLowerCase();
                    String PrimaryRegexPattern = lineSplit[1].replaceAll("^\"|\"$", "");
                    String uriSpace = lineSplit[2];
                    String id = lineSplit[3];
                    String uriLookupEndpoint = lineSplit[4];
                    String title = lineSplit[5];
                    String description = lineSplit[6];
                    dbEntries.put(id.toLowerCase(), new XRefDBEntry(databaseName, PrimaryRegexPattern, uriSpace, id, uriLookupEndpoint, title, description));
                }

            } catch (IOException var11) {
                throw new RuntimeException(var11);
            }
        for (String s : dbEntries.keySet()) {
            if (s.contains("kegg"))
                System.err.println(s);
        }
    }
}
